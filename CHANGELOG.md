# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [6.5.0] - 2024-02-15

### Changed

- Library has been made AOT-compatible.

## [6.4.0] - 2023-11-16

### Changed

- Slightly reduced Gen0 memory allocation.
- If the type wrapped by `PooledObjectWrapper` implements `Microsoft.Extensions.ObjectPool.IResettable` interface,
  then `TryReset` method is invoked when an object returns to pool. `OnResetState` event is triggered anyway. 
- `ResetStateFailureException` can be thrown from `OnResetState` event handlers and from `TryReset` method (see above)
  in order to let the pool know that the object should be released, because it could not be reset properly.
  That type of exception is not logged by ObjectPool, event handlers should perform logging if needed.

## [6.3.0] - 2023-04-13

### Changed

- Restored support for .NET Framework by targeting .NET Standard 2.0.

## [6.2.0] - 2022-12-18

### Added

- Added support for .NET 7.0.

### Removed

- Dropped support for .NET Core 3.1 and .NET Framework 4.7.2.

## [6.1.1] - 2022-10-24

### Fixed

- Issue #17 by @crystalgreen.

## [6.1.0] - 2021-11-20

### Changed

- The project currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.5.2, 4.6.1, 4.7.2.
- The project will support only .NET LTS releases and supported .NET Framework 4.x releases.

### Removed

- The project will not target anymore .NET Standard 2.

## [6.0.0] - 2021-11-03

### Changed

- Library has been annotated with nullable types (MR #32 by @axunonb).

## [5.0.5] - 2021-11-01

### Changed

- Updated embedded LICENSE file.

## [5.0.4] - 2021-09-26

### Changed

- Addressed SonarCloud warnings.

## [5.0.3] - 2021-06-13

### Fixed

- "TotalInstancesCreated" diagnostic was incremented even if factory method threw an exception (issue #13).

## [5.0.2] - 2020-12-26

### Fixed

- Fixed how pooled object state changes from "Unavailable" to "Available" (issue #12).

## [5.0.1] - 2020-11-14

### Fixed

- Microsoft.Extensions.ObjectPool was incorrectly upgraded to 3.x, but 2.x is the correct version.

## [5.0.0] - 2020-10-31

### Changed

- Library has been updated in order to respect most FxCop rules.
- PooledObjectState enumeration entry "Reserved" has been renamed to "Unavailable".
- "continueOnCapturedContext" parameter has been removed from all methods where it appeared.
- "AsyncFactoryMethod" must not accept the "continueOnCapturedContext" parameter anymore.

## [4.0.2] - 2020-02-09

### Changed

- Removed LibLog.

## [4.0.1] - 2019-06-22

### Changed

- Add async support (issue #11).
- Dropped support for .NET Framework 4.0.
- Dropped support for .NET Standard < 2.0.

## [3.2.3] - 2019-05-12

### Changed

- Fixed an exception in PooledObject finalizer (issue #10).
- Fixed how OnValidateObject delegates are invoked (issue #9).

## [3.2.2] - 2017-10-28

### Changed

- Added support for .NET Framework 4.7.1.

## [3.2.1] - 2017-09-30

### Changed

- Added an adapter for Microsoft.Extensions.ObjectPool.

## [3.2.0] - 2017-08-16

### Changed

- Added support for .NET Standard 2.0.
- System.Timer does not seem to be available on .NET Standard 1.0 anymore.
- Added support for .NET Standard 1.2, since it is the minimum version which implements System.Timer.
- Dropped support for .NET Framework 3.5.

## [3.1.1] - 2017-07-03

### Changed

- Object pool now supports an async eviction job (PR #6 by @uliian).
- Timed object pool is now backed by the new eviction system.
- Timed object pool is also available on .NET Standard 1.0 (PR #6 by @uliian).

## [3.1.0] - 2017-06-24

### Changed

- Removed dependency on Thrower.
- Pooled objects can now specify a validation step (PR #4 by @uliian).
- Removed CannotResetStateException class, not needed with new validation step.

## [3.0.3] - 2017-04-08

### Changed

- Added a timed object pool (issue #1).
- OnReleaseResources and OnResetState are now simple actions on PooledObject.

## [3.0.2] - 2017-04-02

### Changed

- Moved core pool buffer into dedicated class: Core.PooledObjectBuffer.

## [3.0.1] - 2017-03-30

### Changed

- Breaking change - Pool does not handle minimum capacity anymore.
- Breaking change - Pooled object ID, state, handle have been moved to PooledObjectInfo property.
- Breaking change - Removed CreatedAt property from PooledMemoryStream and PooledStringBuilder.
- Breaking change - ID property on PooledMemoryStream and PooledStringBuilder is now an int instead of a GUID.
- Default maximum capacity is now 16.

## [2.2.2] - 2017-01-08

### Changed

- Fixed a bug which could produce closed pooled memory streams.
- Converted the project to .NET Core format.
- Updated Thrower to v4.0.6.
- Added unit tests for Portable and .NET Standard 1.1/1.3.

## [2.2.1] - 2016-12-17

### Changed

- Updated Thrower to v4.
- Fixed some mistakes inside nuspec dependencies.

## [2.1.1] - 2016-09-18

### Changed

- BREAKING CHANGE: Removed a feature added by mistake in v2.1.0.

## [2.1.0] - 2016-09-18

### Changed

- Changed default min and max size for MemoryStreamPool: 4KB min, 512KB max.
- Changed default min and max size for StringBuilderPool: 4K char min, 512K char max.
- Created two ad-hoc interfaces for specialized pools.
- BREAKING CHANGE: Moved static properties which controlled specialized pool sizes to the new interfaces.
- Updated Thrower.
- ObjectPool did not respect minimum pool size bound. Now it does.
- When min or max capacity of specialized pools is changed, pool is cleared, if necessary.

## [2.0.5] - 2016-08-23

### Changed

- Fixed wrong name in an exception string.
- Added Id and CreatedAt properties to PooledMemoryStream and PooledStringBuilder.

## [2.0.4] - 2016-08-20

### Changed

- Fixes for new MemoryStream pool.
- Added a MemoryStream pool in the "Specialized" namespace.

## [2.0.2] - 2016-08-20

### Changed

- Added LibLog to .NET 4.x projects.
- Added a DLL compiled for .NET 3.5.
- Added a DLL compiled for .NET Standard 1.3.
- Performance have been improved by 30%.
- Added a StringBuilder pool in the "Specialized" namespace.

## [2.0.1] - 2016-08-07

### Changed

- Library for .NET Standard 1.1.
- Updated NUnit to 3.x branch.

[6.5.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/6.4.0...6.5.0
[6.4.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/6.3.0...6.4.0
[6.3.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/6.2.0...6.3.0
[6.2.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/6.1.1...6.2.0
[6.1.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/6.1.0...6.1.1
[6.1.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/6.0.0...6.1.0
[6.0.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/5.0.5...6.0.0
[5.0.5]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/5.0.4...5.0.5
[5.0.4]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/5.0.3...5.0.4
[5.0.3]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/5.0.2...5.0.3
[5.0.2]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/5.0.1...5.0.2
[5.0.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/5.0.0...5.0.1
[5.0.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/4.0.2...5.0.0
[4.0.2]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/4.0.1...4.0.2
[4.0.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.2.3...4.0.1
[3.2.3]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.2.2...v3.2.3
[3.2.2]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.2.1...v3.2.2
[3.2.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.2.0...v3.2.1
[3.2.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.1.1...v3.2.0
[3.1.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.1.0...v3.1.1
[3.1.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.0.3...v3.1.0
[3.0.3]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.0.2...v3.0.3
[3.0.2]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v3.0.1...v3.0.2
[3.0.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.2.2...v3.0.1
[2.2.2]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.2.1...v2.2.2
[2.2.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.1.1...v2.2.1
[2.1.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.1.0...v2.1.1
[2.1.0]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.0.5...v2.1.0
[2.0.5]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.0.4...v2.0.5
[2.0.4]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.0.2...v2.0.4
[2.0.2]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v2.0.1...v2.0.2
[2.0.1]: https://gitlab.com/pommalabs/codeproject/object-pool/-/compare/v1.10.1...v2.0.1
