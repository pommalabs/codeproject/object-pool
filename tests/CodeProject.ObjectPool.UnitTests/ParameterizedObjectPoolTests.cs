// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;

namespace CodeProject.ObjectPool.UnitTests;

[TestFixture]
internal sealed class ParameterizedObjectPoolTests
{
    [Test]
    public void ShouldChangePoolLimitsIfCorrect()
    {
        var pool = new ParameterizedObjectPool<int, MyPooledObject>();
        Assert.That(pool.MaximumPoolSize, Is.EqualTo(ObjectPool.DefaultPoolMaximumSize));

        pool.MaximumPoolSize *= 2;
        Assert.That(pool.MaximumPoolSize, Is.EqualTo(ObjectPool.DefaultPoolMaximumSize * 2));

        pool.MaximumPoolSize = 2;
        Assert.That(pool.MaximumPoolSize, Is.EqualTo(2));
    }

    [Test]
    public void ShouldHandleClearAfterNoUsage()
    {
        var pool = new ParameterizedObjectPool<int, MyPooledObject>();

        pool.Clear();

        Assert.That(pool.KeysInPoolCount, Is.EqualTo(0));
    }

    [Test]
    public void ShouldHandleClearAfterSomeUsage()
    {
        var pool = new ParameterizedObjectPool<int, MyPooledObject>();

        using (var obj = pool.GetObject(1)) { }

        pool.Clear();

        Assert.That(pool.KeysInPoolCount, Is.EqualTo(0));
    }

    [Test]
    public void ShouldHandleClearAndThenPoolCanBeUsedAgain()
    {
        var pool = new ParameterizedObjectPool<int, MyPooledObject>();

        using (var obj = pool.GetObject(1)) { }

        pool.Clear();

        using (var obj = pool.GetObject(1)) { }

        Assert.That(pool.KeysInPoolCount, Is.EqualTo(1));
    }

    [TestCase(1)]
    [TestCase(5)]
    [TestCase(10)]
    [TestCase(50)]
    [TestCase(100)]
    public async Task ShouldSimplyWork(int maxSize)
    {
        const int KeyCount = 4;
        var pool = new ParameterizedObjectPool<int, MyPooledObject>(maxSize);
        var objectCount = maxSize * KeyCount;
        var objects = new MyPooledObject[objectCount];
        Parallel.For(
            0,
            objectCount,
            i =>
            {
                objects[i] = pool.GetObject(i % KeyCount);
            }
        );
        Parallel.For(
            0,
            objectCount,
            i =>
            {
                objects[i].Dispose();
            }
        );

        await Task.Delay(1000);

        Assert.That(pool.KeysInPoolCount, Is.EqualTo(KeyCount));
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-5)]
    [TestCase(-10)]
    public void ShouldThrowOnMaximumSizeEqualToZeroOrNegative(int maxSize)
    {
        Assert.Throws<ArgumentOutOfRangeException>(
            () => new ParameterizedObjectPool<int, MyPooledObject>(maxSize)
        );
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-5)]
    [TestCase(-10)]
    public void ShouldThrowOnMaximumSizeEqualToZeroOrNegativeOnProperty(int maxSize)
    {
        Assert.Throws<ArgumentOutOfRangeException>(
            () => new ParameterizedObjectPool<int, MyPooledObject> { MaximumPoolSize = maxSize }
        );
    }
}
