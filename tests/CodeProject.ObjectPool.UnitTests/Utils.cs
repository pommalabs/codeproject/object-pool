using Bogus;

namespace CodeProject.ObjectPool.UnitTests;

internal static class Utils
{
    public static Faker Faker { get; } = new Faker();
}
