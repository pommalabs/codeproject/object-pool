// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using CodeProject.ObjectPool.MicrosoftExtensionsAdapter;
using NUnit.Framework;
using Shouldly;

namespace CodeProject.ObjectPool.UnitTests.MicrosoftExtensionsAdapter;

[TestFixture]
internal sealed class ObjectPoolAdapterTests
{
    [Test]
    public void ObjectPoolAdapter_GetAndReturnObject_SameInstance()
    {
        // Arrange
        var pool = ObjectPoolAdapter.Create(
            new ObjectPool<PooledObjectWrapper<List<int>>>(
                () => PooledObjectWrapper.Create(new List<int>())
            )
        );

        var list1 = pool.Get();
        pool.Return(list1);

        // Act
        var list2 = pool.Get();

        // Assert
        list1.ShouldBe(list2);
    }

    [Test]
    public void ObjectPoolAdapterForPooledObject_GetAndReturnObject_SameInstance()
    {
        // Arrange
        var pool = ObjectPoolAdapter.CreateForPooledObject(new ObjectPool<MyPooledObject>());

        var pooledObject1 = pool.Get();
        pool.Return(pooledObject1);

        // Act
        var pooledObject2 = pool.Get();

        // Assert
        pooledObject1.ShouldBe(pooledObject2);
    }
}
