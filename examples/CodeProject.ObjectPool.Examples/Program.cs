// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading;
using CodeProject.ObjectPool.MicrosoftExtensionsAdapter;

namespace CodeProject.ObjectPool.Examples;

/// <summary>
///   Example usages of ObjectPool.
/// </summary>
internal static class Program
{
    public static void ExternalResourceReleaseResource(ExternalExpensiveResource resource)
    {
        // External Resource release code.
    }

    public static void ExternalResourceResetState(ExternalExpensiveResource resource)
    {
        // External Resource reset state code.
    }

    private static ExternalExpensiveResource CreateNewResource()
    {
        return new ExternalExpensiveResource();
    }

    /// <summary>
    ///   Example usages of ObjectPool.
    /// </summary>
    private static void Main()
    {
        // Creating a pool with a maximum size of 25, using custom Factory method to create and
        // instance of ExpensiveResource.
        var pool = new ObjectPool<ExpensiveResource>(
            25,
            () =>
                new ExpensiveResource( /* resource specific initialization */
                )
        );

        using (var resource = pool.GetObject())
        {
            // Using the resource...
            resource.DoStuff();
        } // Exiting the using scope will return the object back to the pool.

        // Creating a pool with wrapper object for managing external resources, that is, classes
        // which cannot inherit from PooledObject.
        var newPool = new ObjectPool<PooledObjectWrapper<ExternalExpensiveResource>>(
            () =>
                new PooledObjectWrapper<ExternalExpensiveResource>(CreateNewResource())
                {
                    OnReleaseResources = ExternalResourceReleaseResource,
                    OnResetState = ExternalResourceResetState
                }
        );

        using (var wrapper = newPool.GetObject())
        {
            // wrapper.InternalResource contains the object that you pooled.
            wrapper.InternalResource.DoOtherStuff();
        } // Exiting the using scope will return the object back to the pool.

        // Creates a pool where objects which have not been used for over 2 seconds will be
        // cleaned up by a dedicated thread.
        var timedPool = new TimedObjectPool<ExpensiveResource>(TimeSpan.FromSeconds(2));

        using (var resource = timedPool.GetObject())
        {
            // Using the resource...
            resource.DoStuff();
        } // Exiting the using scope will return the object back to the pool and record last usage.

        Console.WriteLine($"Timed pool size after 0 seconds: {timedPool.ObjectsInPoolCount}"); // Should be 1
        Thread.Sleep(TimeSpan.FromSeconds(4));
        Console.WriteLine($"Timed pool size after 4 seconds: {timedPool.ObjectsInPoolCount}"); // Should be 0

        // Adapts a timed pool to Microsoft Extensions abstraction.
        var mPool = ObjectPoolAdapter.CreateForPooledObject(timedPool);

        // Example usage of Microsoft pool.
        var mResource = mPool.Get();
        Debug.Assert(mResource != null);
        mPool.Return(mResource);

        // Adapts a new pool to Microsoft Extensions abstraction. This example shows how to
        // adapt when object type does not extend PooledObject.
        var mPool2 = ObjectPoolAdapter.Create(
            new ObjectPool<PooledObjectWrapper<MemoryStream>>(
                () => PooledObjectWrapper.Create(new MemoryStream())
            )
        );

        // Example usage of second Microsoft pool.
        var mResource2 = mPool2.Get();
        Debug.Assert(mResource2 != null);
        mPool2.Return(mResource2);

        Console.Read();
    }
}

internal sealed class ExpensiveResource : PooledObject
{
    public ExpensiveResource()
    {
        OnReleaseResources = () =>
        {
            // Called if the resource needs to be manually cleaned before the memory is reclaimed.
        };

        OnResetState = () =>
        {
            // Called if the resource needs resetting before it is getting back into the pool.
        };
    }

    [SuppressMessage(
        "Performance",
        "CA1822:Mark members as static",
        Justification = "It is just an example method"
    )]
    public void DoStuff()
    {
        // Do some work here, for example.
    }
}

internal sealed class ExternalExpensiveResource
{
    [SuppressMessage(
        "Performance",
        "CA1822:Mark members as static",
        Justification = "It is just an example method"
    )]
    public void DoOtherStuff()
    {
        // Do some work here, for example.
    }
}
