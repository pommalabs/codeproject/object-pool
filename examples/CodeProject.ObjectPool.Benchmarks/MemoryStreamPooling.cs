// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using BenchmarkDotNet.Attributes;
using CodeProject.ObjectPool.Specialized;

namespace CodeProject.ObjectPool.Benchmarks;

[Config(typeof(Program.Config))]
public class MemoryStreamPooling
{
    private readonly IObjectPool<PooledMemoryStream> _objectPool = Specialized
        .MemoryStreamPool
        .Instance;
    private readonly Microsoft.IO.RecyclableMemoryStreamManager _recManager = new();

    [Benchmark(Baseline = true)]
    public long MemoryStreamPool()
    {
        long l;
        using (var x = _objectPool.GetObject())
        {
            l = x.MemoryStream.Length;
        }
        return l;
    }

    [Benchmark]
    public long RecyclableMemoryStreamManager()
    {
        long l;
        using (var x = _recManager.GetStream())
        {
            l = x.Length;
        }
        return l;
    }
}
