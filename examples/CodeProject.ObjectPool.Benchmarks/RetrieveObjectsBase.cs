// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Globalization;
using BenchmarkDotNet.Attributes;
using CodeProject.ObjectPool.MicrosoftExtensionsAdapter;
using Microsoft.Extensions.ObjectPool;

namespace CodeProject.ObjectPool.Benchmarks;

[Config(typeof(Program.Config))]
public class RetrieveObjectsBase
{
    protected readonly Microsoft.Extensions.ObjectPool.ObjectPool<MyResource> AdaptedMicrosoftObjectPool =
        ObjectPoolAdapter.CreateForPooledObject(
            new ObjectPool<MyResource>(21, () => new MyResource(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)))
        );

    protected readonly Microsoft.Extensions.ObjectPool.ObjectPool<MyResource> MicrosoftObjectPool =
        new DefaultObjectPoolProvider().Create(new MyResource.Policy());

    protected readonly ObjectPool<MyResource> ObjectPool =
        new(21, () => new MyResource(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)));

    protected readonly ParameterizedObjectPool<int, MyResource> ParamObjectPool =
        new(21, x => new MyResource(DateTime.UtcNow + "#" + x));

    protected readonly ObjectPool<PooledObjectWrapper<ResettableResource>> ResettableObjectPool =
        new(21,
            () => PooledObjectWrapper.Create(
                new ResettableResource(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture))));

    protected readonly ObjectPool<PooledObjectWrapper<string>> WrappedObjectPool =
        new(21, () => PooledObjectWrapper.Create(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)));

    protected sealed class MyResource : PooledObject
    {
        public MyResource(string value)
        {
            Value = value;
        }

        public string Value { get; }

        public sealed class Policy
            : IPooledObjectPolicy<MyResource>
        {
            public MyResource Create()
            {
                return new MyResource(DateTime.UtcNow.ToString());
            }

            public bool Return(MyResource obj)
            {
                return true;
            }
        }
    }

    protected sealed class ResettableResource : IResettable
    {
        public ResettableResource(string value)
        {
            Value = value;
        }

        public string Value { get; }

        public bool TryReset()
        {
            return true;
        }
    }
}
