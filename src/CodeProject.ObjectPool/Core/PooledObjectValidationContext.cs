// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace CodeProject.ObjectPool.Core;

/// <summary>
///   Contains additional info which might be useful when performing an object validation step.
/// </summary>
public record struct PooledObjectValidationContext
{
    /// <summary>
    ///   Whether an object is going out of the pool or into the pool.
    /// </summary>
    public PooledObjectDirection Direction { get; private set; }

    /// <summary>
    ///   The pooled object which has to be validated.
    /// </summary>
    public PooledObject? PooledObject { get; private set; }

    /// <summary>
    ///   Info about the pooled object which has to be validated.
    /// </summary>
    public PooledObjectInfo? PooledObjectInfo => PooledObject?.PooledObjectInfo;

    /// <summary>
    ///   Used when an object is returning to the pool.
    /// </summary>
    /// <param name="pooledObject">The pooled object which has to be validated.</param>
    internal static PooledObjectValidationContext Inbound(PooledObject pooledObject) =>
        new() { PooledObject = pooledObject, Direction = PooledObjectDirection.Inbound };

    /// <summary>
    ///   Used when an object is going out of the pool.
    /// </summary>
    /// <param name="pooledObject">The pooled object which has to be validated.</param>
    internal static PooledObjectValidationContext Outbound(PooledObject pooledObject) =>
        new() { PooledObject = pooledObject, Direction = PooledObjectDirection.Outbound };
}
