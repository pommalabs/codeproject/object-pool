// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace CodeProject.ObjectPool;

/// <summary>
///   A pool where objects are automatically removed after a period of inactivity.
/// </summary>
/// <typeparam name="T">
///   The type of the object that which will be managed by the pool. The pooled object have to
///   be a sub-class of PooledObject.
/// </typeparam>
public interface ITimedObjectPool<T> : IObjectPool<T> where T : PooledObject
{
    /// <summary>
    ///   When pooled objects have not been used for a time greater than <see cref="Timeout"/>,
    ///   then they will be destroyed by a cleaning task.
    /// </summary>
    TimeSpan Timeout { get; set; }
}
