// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;

namespace CodeProject.ObjectPool.Specialized;

/// <summary>
///   An <see cref="IObjectPool{PooledMemoryStream}"/> ready to be used.
///   <see cref="MemoryStream"/> management can be further configured using the
///   <see cref="MinimumMemoryStreamCapacity"/> and <see cref="MaximumMemoryStreamCapacity"/> properties.
/// </summary>
public sealed class MemoryStreamPool : ObjectPool<PooledMemoryStream>, IMemoryStreamPool
{
    /// <summary>
    ///   Default maximum memory stream capacity. Shared by all <see cref="IMemoryStreamPool"/>
    ///   instances, defaults to 512KB.
    /// </summary>
    public const int DefaultMaximumMemoryStreamCapacity = 512 * 1024;

    /// <summary>
    ///   Default minimum memory stream capacity. Shared by all <see cref="IMemoryStreamPool"/>
    ///   instances, defaults to 4KB.
    /// </summary>
    public const int DefaultMinimumMemoryStreamCapacity = 4 * 1024;

    /// <summary>
    ///   Backing field for <see cref="MaximumMemoryStreamCapacity"/>.
    /// </summary>
    private int _maximumItemCapacity = DefaultMaximumMemoryStreamCapacity;

    /// <summary>
    ///   Backing field for <see cref="MinimumMemoryStreamCapacity"/>
    /// </summary>
    private int _minimumItemCapacity = DefaultMinimumMemoryStreamCapacity;

    /// <summary>
    ///   Builds the specialized pool.
    /// </summary>
    public MemoryStreamPool()
        : base(ObjectPool.DefaultPoolMaximumSize, (Func<PooledMemoryStream>?)null)
    {
        FactoryMethod = () => new PooledMemoryStream(MinimumMemoryStreamCapacity);
    }

    /// <summary>
    ///   Thread-safe pool instance.
    /// </summary>
    public static IMemoryStreamPool Instance { get; } = new MemoryStreamPool();

    /// <summary>
    ///   Maximum capacity a <see cref="MemoryStream"/> might have in order to be able to return
    ///   to pool. Defaults to <see cref="DefaultMaximumMemoryStreamCapacity"/>.
    /// </summary>
    public int MaximumMemoryStreamCapacity
    {
        get { return _maximumItemCapacity; }
        set
        {
            var oldValue = _maximumItemCapacity;
            _maximumItemCapacity = value;
            if (oldValue > value)
            {
                Clear();
            }
        }
    }

    /// <summary>
    ///   Minimum capacity a <see cref="MemoryStream"/> should have when created and this is the
    ///   minimum capacity of all streams stored in the pool. Defaults to <see cref="DefaultMinimumMemoryStreamCapacity"/>.
    /// </summary>
    public int MinimumMemoryStreamCapacity
    {
        get { return _minimumItemCapacity; }
        set
        {
            var oldValue = _minimumItemCapacity;
            _minimumItemCapacity = value;
            if (oldValue < value)
            {
                Clear();
            }
        }
    }
}
