// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Text;
using CodeProject.ObjectPool.Core;

namespace CodeProject.ObjectPool.Specialized;

/// <summary>
///   Pooled object prepared to work with <see cref="StringBuilder"/> instances.
/// </summary>
public class PooledStringBuilder : PooledObject
{
    /// <summary>
    ///   Builds a pooled string builder.
    /// </summary>
    /// <param name="capacity">The capacity of the string builder.</param>
    public PooledStringBuilder(int capacity)
    {
        StringBuilder = new StringBuilder(capacity);

        OnValidateObject += (ctx) =>
        {
            if (ctx.Direction == PooledObjectDirection.Outbound)
            {
                // We validate only inbound objects, because when they are in the pool they
                // cannot change their state.
                return true;
            }

            if (
                PooledObjectInfo.Handle is IStringBuilderPool stringBuilderPool
                && StringBuilder.Capacity > stringBuilderPool.MaximumStringBuilderCapacity
            )
            {
                ObjectPool.Logger?.Invoke(
                    null,
                    "String builder capacity is {StringBuilderCapacity}, while maximum allowed capacity is {MaximumStringBuilderCapacity}",
                    new object[]
                    {
                        StringBuilder.Capacity,
                        stringBuilderPool.MaximumStringBuilderCapacity
                    }
                );
                return false;
            }

            return true; // Object is valid.
        };

        OnResetState += ClearStringBuilder;

        OnReleaseResources += ClearStringBuilder;
    }

    /// <summary>
    ///   The string builder.
    /// </summary>
    public StringBuilder StringBuilder { get; }

    /// <summary>
    ///   Returns a string that represents the current object.
    /// </summary>
    /// <returns>A string that represents the current object.</returns>
    public override string ToString() => StringBuilder.ToString();

    /// <summary>
    ///   Clears the <see cref="StringBuilder"/> property, using specific methods depending on
    ///   the framework for which ObjectPool has been compiled.
    /// </summary>
    protected void ClearStringBuilder()
    {
        StringBuilder.Clear();
    }
}
