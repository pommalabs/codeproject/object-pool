// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.IO;
using CodeProject.ObjectPool.Core;

namespace CodeProject.ObjectPool.Specialized;

/// <summary>
///   Pooled object prepared to work with <see cref="System.IO.MemoryStream"/> instances.
/// </summary>
public class PooledMemoryStream : PooledObject
{
    /// <summary>
    ///   The tracked memory stream.
    /// </summary>
    private readonly TrackedMemoryStream _trackedMemoryStream;

    /// <summary>
    ///   Builds a pooled memory stream.
    /// </summary>
    /// <param name="capacity">The capacity of the backing stream.</param>
    public PooledMemoryStream(int capacity)
    {
        _trackedMemoryStream = new TrackedMemoryStream(capacity) { Parent = this };

        OnValidateObject += (ctx) =>
        {
            if (ctx.Direction == PooledObjectDirection.Outbound)
            {
                // We validate only inbound objects, because when they are in the pool they
                // cannot change their state.
                return true;
            }

            if (
                !_trackedMemoryStream.CanRead
                || !_trackedMemoryStream.CanWrite
                || !_trackedMemoryStream.CanSeek
            )
            {
                ObjectPool.Logger?.Invoke(
                    null,
                    "Memory stream has already been disposed",
                    null
                );
                return false;
            }

            if (PooledObjectInfo.Handle is not IMemoryStreamPool memoryStreamPool)
            {
                ObjectPool.Logger?.Invoke(
                    null,
                    "Pooled object handle does not have expected type",
                    null
                );
                return false;
            }

            if (_trackedMemoryStream.Capacity < memoryStreamPool.MinimumMemoryStreamCapacity)
            {
                ObjectPool.Logger?.Invoke(
                    null,
                    "Memory stream capacity is {StreamCapacity}, while minimum required capacity is {MinimumMemoryStreamCapacity}",
                    new object[]
                    {
                        _trackedMemoryStream.Capacity,
                        memoryStreamPool.MinimumMemoryStreamCapacity
                    }
                );
                return false;
            }
            if (_trackedMemoryStream.Capacity > memoryStreamPool.MaximumMemoryStreamCapacity)
            {
                ObjectPool.Logger?.Invoke(
                    null,
                    "Memory stream capacity is {StreamCapacity}, while maximum allowed capacity is {MaximumMemoryStreamCapacity}",
                    new object[]
                    {
                        _trackedMemoryStream.Capacity,
                        memoryStreamPool.MaximumMemoryStreamCapacity
                    }
                );
                return false;
            }

            return true; // Object is valid.
        };

        OnResetState += () =>
        {
            _trackedMemoryStream.Position = 0L;
            _trackedMemoryStream.SetLength(0L);
        };

        OnReleaseResources += () =>
        {
            _trackedMemoryStream.Parent = null;
            _trackedMemoryStream.Dispose();
        };
    }

    /// <summary>
    ///   The memory stream.
    /// </summary>
    public MemoryStream MemoryStream => _trackedMemoryStream;

    /// <summary>
    ///   Returns a string that represents the current object.
    /// </summary>
    /// <returns>A string that represents the current object.</returns>
    public override string ToString() =>
        $"{nameof(_trackedMemoryStream.Position)}: {_trackedMemoryStream.Position}, {nameof(_trackedMemoryStream.Length)}: {_trackedMemoryStream.Length}";

    private sealed class TrackedMemoryStream : MemoryStream
    {
        public TrackedMemoryStream(int capacity) : base(capacity) { }

        public PooledMemoryStream? Parent { get; set; }

        protected override void Dispose(bool disposing)
        {
            if (disposing && Parent != null)
            {
                Parent.Dispose();
            }
            else
            {
                base.Dispose(disposing);
            }
        }
    }
}
