// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.IO;

namespace CodeProject.ObjectPool.Specialized;

/// <summary>
///   An object pool specialized in <see cref="MemoryStream"/> management.
/// </summary>
public interface IMemoryStreamPool : IObjectPool<PooledMemoryStream>
{
    /// <summary>
    ///   Maximum capacity a <see cref="MemoryStream"/> might have in order to be able to return
    ///   to pool. Defaults to <see cref="MemoryStreamPool.DefaultMaximumMemoryStreamCapacity"/>.
    /// </summary>
    int MaximumMemoryStreamCapacity { get; set; }

    /// <summary>
    ///   Minimum capacity a <see cref="MemoryStream"/> should have when created and this is the
    ///   minimum capacity of all streams stored in the pool. Defaults to <see cref="MemoryStreamPool.DefaultMinimumMemoryStreamCapacity"/>.
    /// </summary>
    int MinimumMemoryStreamCapacity { get; set; }
}
