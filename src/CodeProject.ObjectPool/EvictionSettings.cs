// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace CodeProject.ObjectPool;

/// <summary>
///   Eviction settings.
/// </summary>
public class EvictionSettings
{
    /// <summary>
    ///   Default eviction settings.
    /// </summary>
    public static EvictionSettings Default { get; } = new();

    /// <summary>
    ///   The delay specified when an eviction job is scheduled. Default value is <see cref="TimeSpan.Zero"/>.
    /// </summary>
    public TimeSpan Delay { get; set; } = TimeSpan.Zero;

    /// <summary>
    ///   Whether eviction is enabled or not. By default, eviction is not enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    ///   How frequent should be the eviction job. Default value is one minute.
    /// </summary>
    public TimeSpan Period { get; set; } = TimeSpan.FromMinutes(1);
}
