// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace CodeProject.ObjectPool;

/// <summary>
///   Eviction timer interface, used to abstract over eviction jobs.
/// </summary>
public interface IEvictionTimer : IDisposable
{
    /// <summary>
    ///   Cancels a scheduled eviction action using a ticket returned by <see cref="Schedule(Action, TimeSpan, TimeSpan)"/>.
    /// </summary>
    /// <param name="actionTicket">
    ///   An eviction action ticket, which has been returned by <see cref="Schedule(Action, TimeSpan, TimeSpan)"/>.
    /// </param>
    void Cancel(Guid actionTicket);

    /// <summary>
    ///   Schedules an eviction action.
    /// </summary>
    /// <param name="action">Eviction action.</param>
    /// <param name="delay">Start delay.</param>
    /// <param name="period">Schedule period.</param>
    /// <returns>
    ///   A ticket which identifies the scheduled eviction action, it can be used to cancel the
    ///   scheduled action via <see cref="Cancel(Guid)"/> method.
    /// </returns>
    Guid Schedule(Action action, TimeSpan delay, TimeSpan period);
}
