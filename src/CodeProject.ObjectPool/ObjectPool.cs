/*
 * Generic Object Pool Implementation
 *
 * Implemented by Ofir Makmal, 28/1/2013
 *
 * My Blog: Blogs.microsoft.co.il/blogs/OfirMakmal
 * Email:   Ofir.Makmal@gmail.com
 *
 */

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CodeProject.ObjectPool.Core;

namespace CodeProject.ObjectPool;

/// <summary>
///     Constants for all Object Pools.
/// </summary>
public static class ObjectPool
{
    /// <summary>
    ///     The default maximum size for the pool. It is set to 16.
    /// </summary>
    public const int DefaultPoolMaximumSize = 16;

    /// <summary>
    ///     Logger, which can be linked to any external logging system.
    /// </summary>
    public static Action<Exception?, string, object[]?>? Logger { get; set; }
}

/// <summary>
///     Generic object pool.
/// </summary>
/// <typeparam name="T">
///     The type of the object that which will be managed by the pool. The pooled object have to
///     be a sub-class of PooledObject.
/// </typeparam>
public class ObjectPool<[DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.PublicParameterlessConstructor)] T> : IObjectPool<T>, IObjectPoolHandle where T : PooledObject
{
    #region Finalizer

    /// <summary>
    ///     ObjectPool destructor.
    /// </summary>
    ~ObjectPool()
    {
        // The pool is going down, releasing the resources for all objects in pool.
        Clear();

        // Dispose the eviction timer, if any.
        _evictionTimer?.Dispose();
    }

    #endregion Finalizer

    #region Private Methods

    private T PrepareNewPooledObject(T newObject)
    {
        // Sets the "return to pool" action and other properties in the newly created pooled object.
        newObject.PooledObjectInfo.Id = Interlocked.Increment(ref _lastPooledObjectId);
        newObject.PooledObjectInfo.State = PooledObjectState.Available;
        newObject.PooledObjectInfo.Handle = this;

        return newObject;
    }

    #endregion Private Methods

    #region Constructor and Initialization code

    /// <summary>
    ///     Initializes a new pool with default settings.
    /// </summary>
    public ObjectPool() : this(ObjectPool.DefaultPoolMaximumSize, null, null, null, null) { }

    /// <summary>
    ///     Initializes a new pool with specified maximum pool size.
    /// </summary>
    /// <param name="maximumPoolSize">The maximum pool size limit.</param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="maximumPoolSize" /> is less than or equal to zero.
    /// </exception>
    public ObjectPool(int maximumPoolSize) : this(maximumPoolSize, null, null, null, null) { }

    /// <summary>
    ///     Initializes a new pool with specified factory method.
    /// </summary>
    /// <param name="factoryMethod">The factory method that will be used to create new objects.</param>
    public ObjectPool(Func<T>? factoryMethod)
        : this(ObjectPool.DefaultPoolMaximumSize, factoryMethod, null, null, null)
    {
    }

    /// <summary>
    ///     Initializes a new pool with specified factory method.
    /// </summary>
    /// <param name="asyncFactoryMethod">
    ///     The <see langword="async" /> factory method that will be used to create new objects.
    /// </param>
    public ObjectPool(Func<CancellationToken, Task<T>>? asyncFactoryMethod)
        : this(ObjectPool.DefaultPoolMaximumSize, null, asyncFactoryMethod, null, null)
    {
    }

    /// <summary>
    ///     Initializes a new pool with specified factory method and maximum size.
    /// </summary>
    /// <param name="maximumPoolSize">The maximum pool size limit.</param>
    /// <param name="factoryMethod">The factory method that will be used to create new objects.</param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="maximumPoolSize" /> is less than or equal to zero.
    /// </exception>
    public ObjectPool(int maximumPoolSize, Func<T>? factoryMethod)
        : this(maximumPoolSize, factoryMethod, null, null, null)
    {
    }

    /// <summary>
    ///     Initializes a new pool with specified factory method and maximum size.
    /// </summary>
    /// <param name="maximumPoolSize">The maximum pool size limit.</param>
    /// <param name="asyncFactoryMethod">
    ///     The <see langword="async" /> factory method that will be used to create new objects.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="maximumPoolSize" /> is less than or equal to zero.
    /// </exception>
    public ObjectPool(int maximumPoolSize, Func<CancellationToken, Task<T>>? asyncFactoryMethod)
        : this(maximumPoolSize, null, asyncFactoryMethod, null, null)
    {
    }

    /// <summary>
    ///     Initializes a new pool with specified eviction settings.
    /// </summary>
    /// <param name="evictionSettings">Settings for the validation and eviction job.</param>
    public ObjectPool(EvictionSettings evictionSettings)
        : this(ObjectPool.DefaultPoolMaximumSize, null, null, evictionSettings, null)
    {
    }

    /// <summary>
    ///     Initializes a new pool with specified factory method, maximum size, eviction timer and settings.
    /// </summary>
    /// <param name="maximumPoolSize">The maximum pool size limit.</param>
    /// <param name="factoryMethod">The factory method that will be used to create new objects.</param>
    /// <param name="evictionSettings">Settings for the validation and eviction job.</param>
    /// <param name="evictionTimer">
    ///     The eviction timer used to schedule an <see langword="async" /> validation and eviction job.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="maximumPoolSize" /> is less than or equal to zero.
    /// </exception>
    public ObjectPool(
        int maximumPoolSize,
        Func<T>? factoryMethod,
        EvictionSettings? evictionSettings,
        IEvictionTimer? evictionTimer)
        : this(maximumPoolSize, factoryMethod, null, evictionSettings, evictionTimer)
    {
    }

    /// <summary>
    ///     Initializes a new pool with specified factory method, maximum size, eviction timer and settings.
    /// </summary>
    /// <param name="maximumPoolSize">The maximum pool size limit.</param>
    /// <param name="asyncFactoryMethod">
    ///     The <see langword="async" /> factory method that will be used to create new objects.
    /// </param>
    /// <param name="evictionSettings">Settings for the validation and eviction job.</param>
    /// <param name="evictionTimer">
    ///     The eviction timer used to schedule an <see langword="async" /> validation and eviction job.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="maximumPoolSize" /> is less than or equal to zero.
    /// </exception>
    public ObjectPool(
        int maximumPoolSize,
        Func<CancellationToken, Task<T>>? asyncFactoryMethod,
        EvictionSettings evictionSettings,
        IEvictionTimer? evictionTimer)
        : this(maximumPoolSize, null, asyncFactoryMethod, evictionSettings, evictionTimer)
    {
    }

    /// <summary>
    ///     Initializes a new pool with specified factory method, maximum size, eviction timer and settings.
    /// </summary>
    /// <param name="maximumPoolSize">The maximum pool size limit.</param>
    /// <param name="factoryMethod">The factory method that will be used to create new objects.</param>
    /// <param name="asyncFactoryMethod">
    ///     The <see langword="async" /> factory method that will be used to create new objects.
    /// </param>
    /// <param name="evictionSettings">Settings for the validation and eviction job.</param>
    /// <param name="evictionTimer">
    ///     The eviction timer used to schedule an <see langword="async" /> validation and eviction job.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="maximumPoolSize" /> is less than or equal to zero.
    /// </exception>
    private ObjectPool(
        int maximumPoolSize,
        Func<T>? factoryMethod,
        Func<CancellationToken, Task<T>>? asyncFactoryMethod,
        EvictionSettings? evictionSettings,
        IEvictionTimer? evictionTimer)
    {
        // Preconditions
        if (maximumPoolSize <= 0)
        {
            throw new ArgumentOutOfRangeException(
                nameof(maximumPoolSize),
                ErrorMessages.NegativeOrZeroMaximumPoolSize
            );
        }

        // Throws an exception if the type does not have default constructor - on purpose! We
        // could have added a generic constraint with new (), but we did not want to limit the
        // user and force a parameter-less constructor.
        if (factoryMethod != null)
        {
            FactoryMethod = factoryMethod;
            AsyncFactoryMethod = _ => Task.FromResult(factoryMethod());
        }
        else if (asyncFactoryMethod != null)
        {
            FactoryMethod = () => throw new InvalidOperationException(ErrorMessages.AsyncFactoryForSyncGetObject);
            AsyncFactoryMethod = asyncFactoryMethod;
        }
        else
        {
            FactoryMethod = () => Activator.CreateInstance<T>();
            AsyncFactoryMethod = _ => Task.FromResult(Activator.CreateInstance<T>());
        }

        // Max pool size.
        MaximumPoolSize = maximumPoolSize;

        // Creating a new instance for the Diagnostics class.
        Diagnostics = new ObjectPoolDiagnostics();

        // Use specified timer or create a new one if missing.
        _evictionTimer = evictionTimer ?? new EvictionTimer();
        StartEvictor(evictionSettings ?? EvictionSettings.Default);
    }

    #endregion Constructor and Initialization code

    #region Public Properties

    /// <summary>
    ///     Gets the <see langword="async" /> Factory method that will be used for creating new
    ///     objects with <see langword="async" />/ <see langword="await" /> pattern.
    /// </summary>
    public Func<CancellationToken, Task<T>> AsyncFactoryMethod { get; protected set; }

    /// <summary>
    ///     Gets the Diagnostics class for the current Object Pool, whose goal is to record data
    ///     about how the pool operates. By default, however, an object pool records anything; you
    ///     have to enable it through the <see cref="ObjectPoolDiagnostics.Enabled" /> property.
    /// </summary>
    public ObjectPoolDiagnostics Diagnostics { get; set; }

    /// <summary>
    ///     Gets the Factory method that will be used for creating new objects.
    /// </summary>
    public Func<T> FactoryMethod { get; protected set; }

    /// <summary>
    ///     Gets or sets the maximum number of objects that could be available at the same time in
    ///     the pool.
    /// </summary>
    public int MaximumPoolSize
    {
        get => PooledObjects.Capacity;
        set
        {
            // Preconditions
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(value),
                    ErrorMessages.NegativeOrZeroMaximumPoolSize
                );
            }

            // Resize the pool and destroy exceeding items, if any.
            foreach (var exceedingItem in PooledObjects.Resize(value))
            {
                if (exceedingItem != null)
                {
                    DestroyPooledObject(exceedingItem);
                }
            }
        }
    }

    /// <summary>
    ///     Gets the count of the objects currently in the pool.
    /// </summary>
    public int ObjectsInPoolCount => PooledObjects.Count;

    /// <summary>
    ///     The concurrent buffer containing pooled objects.
    /// </summary>
    protected PooledObjectBuffer<T> PooledObjects { get; } = new();

    #endregion Public Properties

    #region Pool Operations

    /// <summary>
    ///     Clears the pool and destroys each object stored inside it.
    /// </summary>
    public void Clear()
    {
        // Destroy all objects, one by one.
        while (PooledObjects.TryDequeue(out var dequeuedObjectToDestroy))
        {
            DestroyPooledObject(dequeuedObjectToDestroy!);
        }
    }

    /// <summary>
    ///     Gets a monitored object from the pool.
    /// </summary>
    /// <returns>A monitored object from the pool.</returns>
    /// <exception cref="InvalidOperationException">
    ///     If a custom <see langword="async" /> factory method has been specified, this exception
    ///     is thrown in order not to perform a sync-over- <see langword="async" /> operation,
    ///     which might lead to deadlocks.
    /// </exception>
    public T GetObject()
    {
        while (true)
        {
            if (PooledObjects.TryDequeue(out var pooledObject))
            {
                // Object found in pool.
                if (Diagnostics.Enabled)
                {
                    Diagnostics.IncrementPoolObjectHitCount();
                }
            }
            else
            {
                // This should not happen normally, but could be happening when there is stress
                // on the pool. No available objects in pool, create a new one and return it to
                // the caller.
                if (Diagnostics.Enabled)
                {
                    Diagnostics.IncrementPoolObjectMissCount();
                }

                pooledObject = CreatePooledObject();
            }

            if (
                !pooledObject!.ValidateObject(
                    PooledObjectValidationContext.Outbound(pooledObject)
                )
            )
            {
                DestroyPooledObject(pooledObject);
                continue;
            }

            // Change the state of the pooled object, marking it as unavailable. We will mark it
            // as available as soon as the object will return to the pool.
            pooledObject.PooledObjectInfo.State = PooledObjectState.Unavailable;
            return pooledObject;
        }
    }

    /// <summary>
    ///     Gets a monitored object from the pool.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>A monitored object from the pool.</returns>
    public async Task<T> GetObjectAsync(CancellationToken cancellationToken = default)
    {
        while (true)
        {
            if (PooledObjects.TryDequeue(out var pooledObject))
            {
                // Object found in pool.
                if (Diagnostics.Enabled)
                {
                    Diagnostics.IncrementPoolObjectHitCount();
                }
            }
            else
            {
                // This should not happen normally, but could be happening when there is stress
                // on the pool. No available objects in pool, create a new one and return it to
                // the caller.
                if (Diagnostics.Enabled)
                {
                    Diagnostics.IncrementPoolObjectMissCount();
                }

                pooledObject = await CreatePooledObjectAsync(cancellationToken)
                    .ConfigureAwait(false);
            }

            if (
                !pooledObject!.ValidateObject(
                    PooledObjectValidationContext.Outbound(pooledObject)
                )
            )
            {
                DestroyPooledObject(pooledObject);
                continue;
            }

            // Change the state of the pooled object, marking it as unavailable. We will mark it
            // as available as soon as the object will return to the pool.
            pooledObject.PooledObjectInfo.State = PooledObjectState.Unavailable;
            return pooledObject;
        }
    }

    void IObjectPoolHandle.ReturnObjectToPool(
        PooledObject objectToReturnToPool,
        bool reRegisterForFinalization
    )
    {
        // Preconditions
        if (objectToReturnToPool == null)
        {
            throw new ArgumentNullException(nameof(objectToReturnToPool));
        }

        var returnedObject = (T)objectToReturnToPool;

        if (reRegisterForFinalization && Diagnostics.Enabled)
        {
            Diagnostics.IncrementObjectResurrectionCount();
        }

        // Reset the object state (if implemented) before returning it to the pool. If resetting
        // the object have failed, destroy the object.
        if (!returnedObject.ResetState())
        {
            if (Diagnostics.Enabled)
            {
                Diagnostics.IncrementResetStateFailedCount();
            }

            DestroyPooledObject(returnedObject!);
            return;
        }

        // Re-registering for finalization - in case of resurrection (called from Finalize method).
        if (reRegisterForFinalization)
        {
            GC.ReRegisterForFinalize(returnedObject);
        }

        // While adding the object back to the pool, we mark it as available.
        Debug.Assert(returnedObject.PooledObjectInfo.State == PooledObjectState.Unavailable);
        returnedObject.PooledObjectInfo.State = PooledObjectState.Available;

        // Trying to add the object back to the pool.
        if (PooledObjects.TryEnqueue(returnedObject))
        {
            if (Diagnostics.Enabled)
            {
                Diagnostics.IncrementReturnedToPoolCount();
            }
        }
        else
        {
            // The Pool's upper limit has exceeded, there is no need to add this object back
            // into the pool and we can destroy it.
            if (Diagnostics.Enabled)
            {
                Diagnostics.IncrementPoolOverflowCount();
            }

            DestroyPooledObject(returnedObject);
        }
    }

    #endregion Pool Operations

    #region Protected Methods

    /// <summary>
    ///     Used to schedule an <see langword="async" /> validation and eviction job.
    /// </summary>
    private readonly IEvictionTimer _evictionTimer;

    /// <summary>
    ///     Stores the ticket returned by
    ///     <see cref="IEvictionTimer.Schedule(Action, TimeSpan, TimeSpan)" />, in order to be able
    ///     to cancel the scheduled eviction action, if needed.
    /// </summary>
    private Guid _evictionActionTicket;

    /// <summary>
    ///     Keeps track of last pooled object ID.
    /// </summary>
    private int _lastPooledObjectId;

    /// <summary>
    ///     Creates a new pooled object, initializing its info.
    /// </summary>
    /// <returns>A new pooled object.</returns>
    protected virtual T CreatePooledObject()
    {
        var newObject = FactoryMethod();

        if (Diagnostics.Enabled)
        {
            Diagnostics.IncrementObjectsCreatedCount();
        }

        return PrepareNewPooledObject(newObject);
    }

    /// <summary>
    ///     Creates a new pooled object, initializing its info.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>A new pooled object.</returns>
    protected virtual async Task<T> CreatePooledObjectAsync(CancellationToken cancellationToken)
    {
        var newObject = await AsyncFactoryMethod(cancellationToken).ConfigureAwait(false);

        if (Diagnostics.Enabled)
        {
            Diagnostics.IncrementObjectsCreatedCount();
        }

        return PrepareNewPooledObject(newObject);
    }

    /// <summary>
    ///     Destroys given pooled object, disposing its resources.
    /// </summary>
    /// <param name="objectToDestroy">The pooled object that should be destroyed.</param>
    /// <exception cref="ArgumentNullException">
    ///     <paramref name="objectToDestroy" /> is <c>null</c>.
    /// </exception>
    protected void DestroyPooledObject(PooledObject objectToDestroy)
    {
        if (objectToDestroy == null)
        {
            throw new ArgumentNullException(nameof(objectToDestroy));
        }

        // Making sure that the object is only disposed once (in case of application shutting
        // down and we don't control the order of the finalization).
        if (objectToDestroy.PooledObjectInfo.State != PooledObjectState.Disposed)
        {
            // Deterministically release object resources, never mind the result, we are
            // destroying the object.
            objectToDestroy.ReleaseResources();
            objectToDestroy.PooledObjectInfo.State = PooledObjectState.Disposed;

            if (Diagnostics.Enabled)
            {
                Diagnostics.IncrementObjectsDestroyedCount();
            }
        }

#pragma warning disable S3971 // "GC.SuppressFinalize" should not be called

        // The object is being destroyed, resources have been already released
        // deterministically, so we did not need the finalizer to fire.
        GC.SuppressFinalize(objectToDestroy);

#pragma warning restore S3971 // "GC.SuppressFinalize" should not be called
    }

    /// <summary>
    ///     Starts the evictor process, if enabled.
    /// </summary>
    /// <param name="settings">Eviction settings.</param>
    protected void StartEvictor(EvictionSettings settings)
    {
        if (settings?.Enabled == true)
        {
            lock (PooledObjects)
            {
                if (_evictionActionTicket != Guid.Empty)
                {
                    // Cancel previous eviction action.
                    _evictionTimer.Cancel(_evictionActionTicket);
                }

                _evictionActionTicket = _evictionTimer.Schedule(
                    () =>
                    {
                        // All items which are not valid will be destroyed. We make a local copy,
                        // since the buffer might change.
                        foreach (var pooledObject in PooledObjects.ToArray())
                        {
                            if (
                                !pooledObject.ValidateObject(
                                    PooledObjectValidationContext.Outbound(pooledObject)
                                ) && PooledObjects.TryRemove(pooledObject)
                            )
                            {
                                DestroyPooledObject(pooledObject);
                            }
                        }
                    },
                    settings!.Delay,
                    settings!.Period
                );
            }
        }
    }

    #endregion Protected Methods
}
